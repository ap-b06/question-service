package com.quhaad.project.repository;

import com.quhaad.project.core.QuestionSet;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QuestionSetRepository extends JpaRepository<QuestionSet, Integer> {
}
