package com.quhaad.project.controller;

import com.quhaad.project.core.QuestionSet;
import com.quhaad.project.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
    @RequestMapping(path = "/question")
    public class QuestionController {

        @Autowired
        private QuestionService questionService;

        @GetMapping
        public ResponseEntity<List<QuestionSet>> findAll() {
            List<QuestionSet> questionSets = questionService.findAllQuestionSet();
            return new ResponseEntity<>(questionSets, HttpStatus.OK);
        }

        @PostMapping(consumes = {
                MediaType.APPLICATION_JSON_VALUE
        }, produces = {
                MediaType.APPLICATION_JSON_VALUE
        })
        public ResponseEntity<QuestionSet> create(@RequestBody QuestionSet questionSet) {
            QuestionSet newSoul = questionService.registerQuestionSet(questionSet);
            return new ResponseEntity<>(newSoul, HttpStatus.CREATED);
        }

        @GetMapping("/{queryId}")
        public ResponseEntity<QuestionSet> findById(@PathVariable int queryId) {
            if(!questionService.findQuestionSet(queryId).isPresent()) {
                throw new NoSuchElementException("Question set not found!");
            }else {
                return new ResponseEntity<>(questionService.findQuestionSet(queryId).get(), HttpStatus.OK);
            }
        }

        @DeleteMapping("/{queryId}")
        public ResponseEntity delete(@PathVariable int queryId) {
            questionService.deleteQuestionSet(queryId);
            return new ResponseEntity<>(HttpStatus.OK);
        }
}
