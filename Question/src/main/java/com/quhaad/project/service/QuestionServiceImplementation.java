package com.quhaad.project.service;

import com.quhaad.project.core.Question;
import com.quhaad.project.core.QuestionSet;
import org.springframework.stereotype.Service;
import com.quhaad.project.repository.QuestionSetRepository;

import java.util.List;
import java.util.Optional;

@Service
public class QuestionServiceImplementation implements QuestionService {

    private final QuestionSetRepository questionSetRepository;

    public QuestionServiceImplementation(QuestionSetRepository questionSetRepository) {
        this.questionSetRepository = questionSetRepository;
    }

    /**
     * This method is used to find all question set
     * @return
     */
    @Override
    public List<QuestionSet> findAllQuestionSet() {
        return questionSetRepository.findAll();
    }

    /**
     * This method is used to find question set by id
     * @param questionSetId
     * @return
     */
    @Override
    public Optional<QuestionSet> findQuestionSet(int questionSetId) {
        return questionSetRepository.findById(questionSetId);
    }

    /**
     * This method is used to create new question set
     * @param questionSet
     * @return
     */
    @Override
    public QuestionSet registerQuestionSet(QuestionSet questionSet) {
        return questionSetRepository.save(questionSet);
    }

    /**
     * This method is used to delete a question set by question set id
     * @param questionSetId
     */
    @Override
    public void deleteQuestionSet(int questionSetId) {
        questionSetRepository.deleteById(questionSetId);
    }
}
