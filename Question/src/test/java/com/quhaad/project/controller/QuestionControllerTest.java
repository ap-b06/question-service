package com.quhaad.project.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.InstanceOfAssertFactories.map;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.quhaad.project.controller.QuestionController;
import com.quhaad.project.core.*;
import com.quhaad.project.service.*;

//@ExtendWith(MockitoExtension.class)
@WebMvcTest(controllers = QuestionController.class)
public class QuestionControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private QuestionServiceImplementation questionService;

    @Test
    public void whenQuestionURIIsAccessedShouldHandledByFindAll() throws Exception{
        mockMvc.perform(get("/question/"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("findAll"));
    }

    @Test
    public void whenCreateURLIsAccessedShouldAddAQuestionSet() throws Exception {
        QuestionSet questionSet = new QuestionSet();
        questionSet.setQuestionSetName("Mock Question");
        List<Question> questionList = new ArrayList<>();
        Question question = new Question();
        question.setQuestionText("This is a test.");
        question.setQuestionMediaUrl("testing.com");
        question.setAnswer1("A");
        question.setAnswer2("B");
        question.setAnswer3("C");
        question.setAnswer4("D");
        question.setCorrectAnswer("A");
        question.setTimeLimit(10);
        questionList.add(question);
        questionSet.setListOfQuestions(questionList);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJSON = ow.writeValueAsString(questionSet);

        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", java.nio.charset.Charset.forName("UTF-8"));
        MockHttpServletRequestBuilder request = post("/question/");
        request.content(requestJSON);
        request.accept(MEDIA_TYPE_JSON_UTF8);
        request.contentType(MEDIA_TYPE_JSON_UTF8);

        mockMvc.perform(request
                .content(requestJSON))
                .andExpect(status().isCreated());
    }

    @Test
    public void whenDeleteQuestionURIIsAccessedItShouldHandledByDelete() throws Exception{
        QuestionSet questionSet = new QuestionSet();
        int questionSetId = questionSet.getQuestionSetId();

        mockMvc.perform(delete("/question/{queryId}", questionSetId))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("delete"));
    }

//    @InjectMocks
//    QuestionController questionController;
//
//    @Mock
//    QuestionServiceImplementation questionService;
//
//    @Test
//    public void testCreate() {
//        MockHttpServletRequest request = new MockHttpServletRequest();
//        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
//
//        when(questionService.registerQuestionSet(any(QuestionSet.class))).thenReturn(any(QuestionSet.class));
//
//        QuestionSet questionSet =  new QuestionSet();
//        ResponseEntity<QuestionSet> responseEntity = questionController.create(questionSet);
//
//        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(201);
//    }
//
//    @Test
//    public void testFindAll() {
//        List<Question> questionList = new ArrayList<>();
//        QuestionSet questionSet1 = new QuestionSet();
//        questionSet1.setQuestionSetName("First Question Set");
//        questionSet1.setListOfQuestions(questionList);
//
//        QuestionSet questionSet2 = new QuestionSet();
//        questionSet2.setQuestionSetName("Second Question Set");
//        questionSet2.setListOfQuestions(questionList);
//
//        List<QuestionSet> allQuestionSet = new ArrayList<>();
//        allQuestionSet.add(questionSet1);
//        allQuestionSet.add(questionSet2);
//
//        when(questionService.findAllQuestionSet()).thenReturn(allQuestionSet);
//    }
}